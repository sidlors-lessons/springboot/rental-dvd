package mx.sidlors.rental.dvd;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import mx.sidlors.dvd.rental.RentalDvdApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ActiveProfiles("test")

public class RentalDvdApplicationTest {
    @Test
	public void contextLoads() {
	}

}