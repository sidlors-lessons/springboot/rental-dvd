package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Payment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Payment   {
  @JsonProperty("payment_id")
  private Integer paymentId = null;

  @JsonProperty("customer_id")
  private Integer customerId = null;

  @JsonProperty("staff_id")
  private Integer staffId = null;

  @JsonProperty("rental_id")
  private Integer rentalId = null;

  @JsonProperty("amount")
  private BigDecimal amount = null;

  @JsonProperty("payment_date")
  private String paymentDate = null;

  public Payment paymentId(Integer paymentId) {
    this.paymentId = paymentId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return paymentId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(Integer paymentId) {
    this.paymentId = paymentId;
  }

  public Payment customerId(Integer customerId) {
    this.customerId = customerId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `customer.customer_id`.<fk table='customer' column='customer_id'/>
   * @return customerId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `customer.customer_id`.<fk table='customer' column='customer_id'/>")
  @NotNull


  public Integer getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Integer customerId) {
    this.customerId = customerId;
  }

  public Payment staffId(Integer staffId) {
    this.staffId = staffId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>
   * @return staffId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>")
  @NotNull


  public Integer getStaffId() {
    return staffId;
  }

  public void setStaffId(Integer staffId) {
    this.staffId = staffId;
  }

  public Payment rentalId(Integer rentalId) {
    this.rentalId = rentalId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `rental.rental_id`.<fk table='rental' column='rental_id'/>
   * @return rentalId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `rental.rental_id`.<fk table='rental' column='rental_id'/>")
  @NotNull


  public Integer getRentalId() {
    return rentalId;
  }

  public void setRentalId(Integer rentalId) {
    this.rentalId = rentalId;
  }

  public Payment amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Payment paymentDate(String paymentDate) {
    this.paymentDate = paymentDate;
    return this;
  }

  /**
   * Get paymentDate
   * @return paymentDate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(String paymentDate) {
    this.paymentDate = paymentDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Payment payment = (Payment) o;
    return Objects.equals(this.paymentId, payment.paymentId) &&
        Objects.equals(this.customerId, payment.customerId) &&
        Objects.equals(this.staffId, payment.staffId) &&
        Objects.equals(this.rentalId, payment.rentalId) &&
        Objects.equals(this.amount, payment.amount) &&
        Objects.equals(this.paymentDate, payment.paymentDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentId, customerId, staffId, rentalId, amount, paymentDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Payment {\n");
    
    sb.append("    paymentId: ").append(toIndentedString(paymentId)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    staffId: ").append(toIndentedString(staffId)).append("\n");
    sb.append("    rentalId: ").append(toIndentedString(rentalId)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    paymentDate: ").append(toIndentedString(paymentDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

