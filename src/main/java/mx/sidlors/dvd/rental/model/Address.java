package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Address
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Address   {
  @JsonProperty("address_id")
  private Integer addressId = null;

  @JsonProperty("address")
  private String address = null;

  @JsonProperty("address2")
  private String address2 = null;

  @JsonProperty("district")
  private String district = null;

  @JsonProperty("city_id")
  private Integer cityId = null;

  @JsonProperty("postal_code")
  private String postalCode = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public Address addressId(Integer addressId) {
    this.addressId = addressId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return addressId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getAddressId() {
    return addressId;
  }

  public void setAddressId(Integer addressId) {
    this.addressId = addressId;
  }

  public Address address(String address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Size(max=50) 
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Address address2(String address2) {
    this.address2 = address2;
    return this;
  }

  /**
   * Get address2
   * @return address2
  **/
  @ApiModelProperty(value = "")

@Size(max=50) 
  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public Address district(String district) {
    this.district = district;
    return this;
  }

  /**
   * Get district
   * @return district
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Size(max=20) 
  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  public Address cityId(Integer cityId) {
    this.cityId = cityId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `city.city_id`.<fk table='city' column='city_id'/>
   * @return cityId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `city.city_id`.<fk table='city' column='city_id'/>")
  @NotNull


  public Integer getCityId() {
    return cityId;
  }

  public void setCityId(Integer cityId) {
    this.cityId = cityId;
  }

  public Address postalCode(String postalCode) {
    this.postalCode = postalCode;
    return this;
  }

  /**
   * Get postalCode
   * @return postalCode
  **/
  @ApiModelProperty(value = "")

@Size(max=10) 
  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public Address phone(String phone) {
    this.phone = phone;
    return this;
  }

  /**
   * Get phone
   * @return phone
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Size(max=20) 
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Address lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(this.addressId, address.addressId) &&
        Objects.equals(this.address, address.address) &&
        Objects.equals(this.address2, address.address2) &&
        Objects.equals(this.district, address.district) &&
        Objects.equals(this.cityId, address.cityId) &&
        Objects.equals(this.postalCode, address.postalCode) &&
        Objects.equals(this.phone, address.phone) &&
        Objects.equals(this.lastUpdate, address.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressId, address, address2, district, cityId, postalCode, phone, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    addressId: ").append(toIndentedString(addressId)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    address2: ").append(toIndentedString(address2)).append("\n");
    sb.append("    district: ").append(toIndentedString(district)).append("\n");
    sb.append("    cityId: ").append(toIndentedString(cityId)).append("\n");
    sb.append("    postalCode: ").append(toIndentedString(postalCode)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

