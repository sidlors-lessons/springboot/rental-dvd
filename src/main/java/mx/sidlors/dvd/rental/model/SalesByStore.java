package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SalesByStore
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class SalesByStore   {
  @JsonProperty("store")
  private String store = null;

  @JsonProperty("manager")
  private String manager = null;

  @JsonProperty("total_sales")
  private BigDecimal totalSales = null;

  public SalesByStore store(String store) {
    this.store = store;
    return this;
  }

  /**
   * Get store
   * @return store
  **/
  @ApiModelProperty(value = "")


  public String getStore() {
    return store;
  }

  public void setStore(String store) {
    this.store = store;
  }

  public SalesByStore manager(String manager) {
    this.manager = manager;
    return this;
  }

  /**
   * Get manager
   * @return manager
  **/
  @ApiModelProperty(value = "")


  public String getManager() {
    return manager;
  }

  public void setManager(String manager) {
    this.manager = manager;
  }

  public SalesByStore totalSales(BigDecimal totalSales) {
    this.totalSales = totalSales;
    return this;
  }

  /**
   * Get totalSales
   * @return totalSales
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalSales() {
    return totalSales;
  }

  public void setTotalSales(BigDecimal totalSales) {
    this.totalSales = totalSales;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SalesByStore salesByStore = (SalesByStore) o;
    return Objects.equals(this.store, salesByStore.store) &&
        Objects.equals(this.manager, salesByStore.manager) &&
        Objects.equals(this.totalSales, salesByStore.totalSales);
  }

  @Override
  public int hashCode() {
    return Objects.hash(store, manager, totalSales);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SalesByStore {\n");
    
    sb.append("    store: ").append(toIndentedString(store)).append("\n");
    sb.append("    manager: ").append(toIndentedString(manager)).append("\n");
    sb.append("    totalSales: ").append(toIndentedString(totalSales)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

