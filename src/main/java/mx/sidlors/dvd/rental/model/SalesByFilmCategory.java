package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SalesByFilmCategory
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class SalesByFilmCategory   {
  @JsonProperty("category")
  private String category = null;

  @JsonProperty("total_sales")
  private BigDecimal totalSales = null;

  public SalesByFilmCategory category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(value = "")

@Size(max=25) 
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public SalesByFilmCategory totalSales(BigDecimal totalSales) {
    this.totalSales = totalSales;
    return this;
  }

  /**
   * Get totalSales
   * @return totalSales
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTotalSales() {
    return totalSales;
  }

  public void setTotalSales(BigDecimal totalSales) {
    this.totalSales = totalSales;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SalesByFilmCategory salesByFilmCategory = (SalesByFilmCategory) o;
    return Objects.equals(this.category, salesByFilmCategory.category) &&
        Objects.equals(this.totalSales, salesByFilmCategory.totalSales);
  }

  @Override
  public int hashCode() {
    return Objects.hash(category, totalSales);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SalesByFilmCategory {\n");
    
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    totalSales: ").append(toIndentedString(totalSales)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

