package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Rental
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Rental   {
  @JsonProperty("rental_id")
  private Integer rentalId = null;

  @JsonProperty("rental_date")
  private String rentalDate = null;

  @JsonProperty("inventory_id")
  private Integer inventoryId = null;

  @JsonProperty("customer_id")
  private Integer customerId = null;

  @JsonProperty("return_date")
  private String returnDate = null;

  @JsonProperty("staff_id")
  private Integer staffId = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public Rental rentalId(Integer rentalId) {
    this.rentalId = rentalId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return rentalId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getRentalId() {
    return rentalId;
  }

  public void setRentalId(Integer rentalId) {
    this.rentalId = rentalId;
  }

  public Rental rentalDate(String rentalDate) {
    this.rentalDate = rentalDate;
    return this;
  }

  /**
   * Get rentalDate
   * @return rentalDate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getRentalDate() {
    return rentalDate;
  }

  public void setRentalDate(String rentalDate) {
    this.rentalDate = rentalDate;
  }

  public Rental inventoryId(Integer inventoryId) {
    this.inventoryId = inventoryId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `inventory.inventory_id`.<fk table='inventory' column='inventory_id'/>
   * @return inventoryId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `inventory.inventory_id`.<fk table='inventory' column='inventory_id'/>")
  @NotNull


  public Integer getInventoryId() {
    return inventoryId;
  }

  public void setInventoryId(Integer inventoryId) {
    this.inventoryId = inventoryId;
  }

  public Rental customerId(Integer customerId) {
    this.customerId = customerId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `customer.customer_id`.<fk table='customer' column='customer_id'/>
   * @return customerId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `customer.customer_id`.<fk table='customer' column='customer_id'/>")
  @NotNull


  public Integer getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Integer customerId) {
    this.customerId = customerId;
  }

  public Rental returnDate(String returnDate) {
    this.returnDate = returnDate;
    return this;
  }

  /**
   * Get returnDate
   * @return returnDate
  **/
  @ApiModelProperty(value = "")


  public String getReturnDate() {
    return returnDate;
  }

  public void setReturnDate(String returnDate) {
    this.returnDate = returnDate;
  }

  public Rental staffId(Integer staffId) {
    this.staffId = staffId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>
   * @return staffId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>")
  @NotNull


  public Integer getStaffId() {
    return staffId;
  }

  public void setStaffId(Integer staffId) {
    this.staffId = staffId;
  }

  public Rental lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Rental rental = (Rental) o;
    return Objects.equals(this.rentalId, rental.rentalId) &&
        Objects.equals(this.rentalDate, rental.rentalDate) &&
        Objects.equals(this.inventoryId, rental.inventoryId) &&
        Objects.equals(this.customerId, rental.customerId) &&
        Objects.equals(this.returnDate, rental.returnDate) &&
        Objects.equals(this.staffId, rental.staffId) &&
        Objects.equals(this.lastUpdate, rental.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rentalId, rentalDate, inventoryId, customerId, returnDate, staffId, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Rental {\n");
    
    sb.append("    rentalId: ").append(toIndentedString(rentalId)).append("\n");
    sb.append("    rentalDate: ").append(toIndentedString(rentalDate)).append("\n");
    sb.append("    inventoryId: ").append(toIndentedString(inventoryId)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    returnDate: ").append(toIndentedString(returnDate)).append("\n");
    sb.append("    staffId: ").append(toIndentedString(staffId)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

