package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ActorInfo
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class ActorInfo   {
  @JsonProperty("actor_id")
  private Integer actorId = null;

  @JsonProperty("first_name")
  private String firstName = null;

  @JsonProperty("last_name")
  private String lastName = null;

  @JsonProperty("film_info")
  private String filmInfo = null;

  public ActorInfo actorId(Integer actorId) {
    this.actorId = actorId;
    return this;
  }

  /**
   * Get actorId
   * @return actorId
  **/
  @ApiModelProperty(value = "")


  public Integer getActorId() {
    return actorId;
  }

  public void setActorId(Integer actorId) {
    this.actorId = actorId;
  }

  public ActorInfo firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * Get firstName
   * @return firstName
  **/
  @ApiModelProperty(value = "")

@Size(max=45) 
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public ActorInfo lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Get lastName
   * @return lastName
  **/
  @ApiModelProperty(value = "")

@Size(max=45) 
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public ActorInfo filmInfo(String filmInfo) {
    this.filmInfo = filmInfo;
    return this;
  }

  /**
   * Get filmInfo
   * @return filmInfo
  **/
  @ApiModelProperty(value = "")


  public String getFilmInfo() {
    return filmInfo;
  }

  public void setFilmInfo(String filmInfo) {
    this.filmInfo = filmInfo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActorInfo actorInfo = (ActorInfo) o;
    return Objects.equals(this.actorId, actorInfo.actorId) &&
        Objects.equals(this.firstName, actorInfo.firstName) &&
        Objects.equals(this.lastName, actorInfo.lastName) &&
        Objects.equals(this.filmInfo, actorInfo.filmInfo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(actorId, firstName, lastName, filmInfo);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ActorInfo {\n");
    
    sb.append("    actorId: ").append(toIndentedString(actorId)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    filmInfo: ").append(toIndentedString(filmInfo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

