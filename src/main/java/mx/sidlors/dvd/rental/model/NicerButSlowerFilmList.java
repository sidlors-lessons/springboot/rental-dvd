package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NicerButSlowerFilmList
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class NicerButSlowerFilmList   {
  @JsonProperty("fid")
  private Integer fid = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("category")
  private String category = null;

  @JsonProperty("price")
  private BigDecimal price = null;

  @JsonProperty("length")
  private Integer length = null;

  /**
   * Gets or Sets rating
   */
  public enum RatingEnum {
    G("G"),
    
    PG("PG"),
    
    PG_13("PG-13"),
    
    R("R"),
    
    NC_17("NC-17");

    private String value;

    RatingEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RatingEnum fromValue(String text) {
      for (RatingEnum b : RatingEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("rating")
  private RatingEnum rating = null;

  @JsonProperty("actors")
  private String actors = null;

  public NicerButSlowerFilmList fid(Integer fid) {
    this.fid = fid;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return fid
  **/
  @ApiModelProperty(value = "Note: This is a Primary Key.<pk/>")


  public Integer getFid() {
    return fid;
  }

  public void setFid(Integer fid) {
    this.fid = fid;
  }

  public NicerButSlowerFilmList title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  **/
  @ApiModelProperty(value = "")

@Size(max=255) 
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public NicerButSlowerFilmList description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public NicerButSlowerFilmList category(String category) {
    this.category = category;
    return this;
  }

  /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(value = "")

@Size(max=25) 
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public NicerButSlowerFilmList price(BigDecimal price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public NicerButSlowerFilmList length(Integer length) {
    this.length = length;
    return this;
  }

  /**
   * Get length
   * @return length
  **/
  @ApiModelProperty(value = "")


  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public NicerButSlowerFilmList rating(RatingEnum rating) {
    this.rating = rating;
    return this;
  }

  /**
   * Get rating
   * @return rating
  **/
  @ApiModelProperty(value = "")


  public RatingEnum getRating() {
    return rating;
  }

  public void setRating(RatingEnum rating) {
    this.rating = rating;
  }

  public NicerButSlowerFilmList actors(String actors) {
    this.actors = actors;
    return this;
  }

  /**
   * Get actors
   * @return actors
  **/
  @ApiModelProperty(value = "")


  public String getActors() {
    return actors;
  }

  public void setActors(String actors) {
    this.actors = actors;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NicerButSlowerFilmList nicerButSlowerFilmList = (NicerButSlowerFilmList) o;
    return Objects.equals(this.fid, nicerButSlowerFilmList.fid) &&
        Objects.equals(this.title, nicerButSlowerFilmList.title) &&
        Objects.equals(this.description, nicerButSlowerFilmList.description) &&
        Objects.equals(this.category, nicerButSlowerFilmList.category) &&
        Objects.equals(this.price, nicerButSlowerFilmList.price) &&
        Objects.equals(this.length, nicerButSlowerFilmList.length) &&
        Objects.equals(this.rating, nicerButSlowerFilmList.rating) &&
        Objects.equals(this.actors, nicerButSlowerFilmList.actors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fid, title, description, category, price, length, rating, actors);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NicerButSlowerFilmList {\n");
    
    sb.append("    fid: ").append(toIndentedString(fid)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    length: ").append(toIndentedString(length)).append("\n");
    sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
    sb.append("    actors: ").append(toIndentedString(actors)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

