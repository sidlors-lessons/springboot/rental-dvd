package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Store
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Store   {
  @JsonProperty("store_id")
  private Integer storeId = null;

  @JsonProperty("manager_staff_id")
  private Integer managerStaffId = null;

  @JsonProperty("address_id")
  private Integer addressId = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public Store storeId(Integer storeId) {
    this.storeId = storeId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return storeId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }

  public Store managerStaffId(Integer managerStaffId) {
    this.managerStaffId = managerStaffId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>
   * @return managerStaffId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `staff.staff_id`.<fk table='staff' column='staff_id'/>")
  @NotNull


  public Integer getManagerStaffId() {
    return managerStaffId;
  }

  public void setManagerStaffId(Integer managerStaffId) {
    this.managerStaffId = managerStaffId;
  }

  public Store addressId(Integer addressId) {
    this.addressId = addressId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `address.address_id`.<fk table='address' column='address_id'/>
   * @return addressId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `address.address_id`.<fk table='address' column='address_id'/>")
  @NotNull


  public Integer getAddressId() {
    return addressId;
  }

  public void setAddressId(Integer addressId) {
    this.addressId = addressId;
  }

  public Store lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Store store = (Store) o;
    return Objects.equals(this.storeId, store.storeId) &&
        Objects.equals(this.managerStaffId, store.managerStaffId) &&
        Objects.equals(this.addressId, store.addressId) &&
        Objects.equals(this.lastUpdate, store.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(storeId, managerStaffId, addressId, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Store {\n");
    
    sb.append("    storeId: ").append(toIndentedString(storeId)).append("\n");
    sb.append("    managerStaffId: ").append(toIndentedString(managerStaffId)).append("\n");
    sb.append("    addressId: ").append(toIndentedString(addressId)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

