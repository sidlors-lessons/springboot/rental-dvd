package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Film
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Film   {
  @JsonProperty("film_id")
  private Integer filmId = null;

  @JsonProperty("title")
  private String title = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("release_year")
  private Integer releaseYear = null;

  @JsonProperty("language_id")
  private Integer languageId = null;

  @JsonProperty("rental_duration")
  private Integer rentalDuration = null;

  @JsonProperty("rental_rate")
  private BigDecimal rentalRate = null;

  @JsonProperty("length")
  private Integer length = null;

  @JsonProperty("replacement_cost")
  private BigDecimal replacementCost = null;

  /**
   * Gets or Sets rating
   */
  public enum RatingEnum {
    G("G"),
    
    PG("PG"),
    
    PG_13("PG-13"),
    
    R("R"),
    
    NC_17("NC-17");

    private String value;

    RatingEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RatingEnum fromValue(String text) {
      for (RatingEnum b : RatingEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("rating")
  private RatingEnum rating = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  @JsonProperty("special_features")
  private String specialFeatures = null;

  @JsonProperty("fulltext")
  private String fulltext = null;

  public Film filmId(Integer filmId) {
    this.filmId = filmId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return filmId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getFilmId() {
    return filmId;
  }

  public void setFilmId(Integer filmId) {
    this.filmId = filmId;
  }

  public Film title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Size(max=255) 
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Film description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Film releaseYear(Integer releaseYear) {
    this.releaseYear = releaseYear;
    return this;
  }

  /**
   * Get releaseYear
   * @return releaseYear
  **/
  @ApiModelProperty(value = "")


  public Integer getReleaseYear() {
    return releaseYear;
  }

  public void setReleaseYear(Integer releaseYear) {
    this.releaseYear = releaseYear;
  }

  public Film languageId(Integer languageId) {
    this.languageId = languageId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `language.language_id`.<fk table='language' column='language_id'/>
   * @return languageId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `language.language_id`.<fk table='language' column='language_id'/>")
  @NotNull


  public Integer getLanguageId() {
    return languageId;
  }

  public void setLanguageId(Integer languageId) {
    this.languageId = languageId;
  }

  public Film rentalDuration(Integer rentalDuration) {
    this.rentalDuration = rentalDuration;
    return this;
  }

  /**
   * Get rentalDuration
   * @return rentalDuration
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getRentalDuration() {
    return rentalDuration;
  }

  public void setRentalDuration(Integer rentalDuration) {
    this.rentalDuration = rentalDuration;
  }

  public Film rentalRate(BigDecimal rentalRate) {
    this.rentalRate = rentalRate;
    return this;
  }

  /**
   * Get rentalRate
   * @return rentalRate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public BigDecimal getRentalRate() {
    return rentalRate;
  }

  public void setRentalRate(BigDecimal rentalRate) {
    this.rentalRate = rentalRate;
  }

  public Film length(Integer length) {
    this.length = length;
    return this;
  }

  /**
   * Get length
   * @return length
  **/
  @ApiModelProperty(value = "")


  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public Film replacementCost(BigDecimal replacementCost) {
    this.replacementCost = replacementCost;
    return this;
  }

  /**
   * Get replacementCost
   * @return replacementCost
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public BigDecimal getReplacementCost() {
    return replacementCost;
  }

  public void setReplacementCost(BigDecimal replacementCost) {
    this.replacementCost = replacementCost;
  }

  public Film rating(RatingEnum rating) {
    this.rating = rating;
    return this;
  }

  /**
   * Get rating
   * @return rating
  **/
  @ApiModelProperty(value = "")


  public RatingEnum getRating() {
    return rating;
  }

  public void setRating(RatingEnum rating) {
    this.rating = rating;
  }

  public Film lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public Film specialFeatures(String specialFeatures) {
    this.specialFeatures = specialFeatures;
    return this;
  }

  /**
   * Get specialFeatures
   * @return specialFeatures
  **/
  @ApiModelProperty(value = "")


  public String getSpecialFeatures() {
    return specialFeatures;
  }

  public void setSpecialFeatures(String specialFeatures) {
    this.specialFeatures = specialFeatures;
  }

  public Film fulltext(String fulltext) {
    this.fulltext = fulltext;
    return this;
  }

  /**
   * Get fulltext
   * @return fulltext
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getFulltext() {
    return fulltext;
  }

  public void setFulltext(String fulltext) {
    this.fulltext = fulltext;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Film film = (Film) o;
    return Objects.equals(this.filmId, film.filmId) &&
        Objects.equals(this.title, film.title) &&
        Objects.equals(this.description, film.description) &&
        Objects.equals(this.releaseYear, film.releaseYear) &&
        Objects.equals(this.languageId, film.languageId) &&
        Objects.equals(this.rentalDuration, film.rentalDuration) &&
        Objects.equals(this.rentalRate, film.rentalRate) &&
        Objects.equals(this.length, film.length) &&
        Objects.equals(this.replacementCost, film.replacementCost) &&
        Objects.equals(this.rating, film.rating) &&
        Objects.equals(this.lastUpdate, film.lastUpdate) &&
        Objects.equals(this.specialFeatures, film.specialFeatures) &&
        Objects.equals(this.fulltext, film.fulltext);
  }

  @Override
  public int hashCode() {
    return Objects.hash(filmId, title, description, releaseYear, languageId, rentalDuration, rentalRate, length, replacementCost, rating, lastUpdate, specialFeatures, fulltext);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Film {\n");
    
    sb.append("    filmId: ").append(toIndentedString(filmId)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    releaseYear: ").append(toIndentedString(releaseYear)).append("\n");
    sb.append("    languageId: ").append(toIndentedString(languageId)).append("\n");
    sb.append("    rentalDuration: ").append(toIndentedString(rentalDuration)).append("\n");
    sb.append("    rentalRate: ").append(toIndentedString(rentalRate)).append("\n");
    sb.append("    length: ").append(toIndentedString(length)).append("\n");
    sb.append("    replacementCost: ").append(toIndentedString(replacementCost)).append("\n");
    sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("    specialFeatures: ").append(toIndentedString(specialFeatures)).append("\n");
    sb.append("    fulltext: ").append(toIndentedString(fulltext)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

