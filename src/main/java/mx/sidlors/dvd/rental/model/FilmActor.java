package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * FilmActor
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class FilmActor   {
  @JsonProperty("actor_id")
  private Integer actorId = null;

  @JsonProperty("film_id")
  private Integer filmId = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public FilmActor actorId(Integer actorId) {
    this.actorId = actorId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/> This is a Foreign Key to `actor.actor_id`.<fk table='actor' column='actor_id'/>
   * @return actorId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/> This is a Foreign Key to `actor.actor_id`.<fk table='actor' column='actor_id'/>")
  @NotNull


  public Integer getActorId() {
    return actorId;
  }

  public void setActorId(Integer actorId) {
    this.actorId = actorId;
  }

  public FilmActor filmId(Integer filmId) {
    this.filmId = filmId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/> This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>
   * @return filmId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/> This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>")
  @NotNull


  public Integer getFilmId() {
    return filmId;
  }

  public void setFilmId(Integer filmId) {
    this.filmId = filmId;
  }

  public FilmActor lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FilmActor filmActor = (FilmActor) o;
    return Objects.equals(this.actorId, filmActor.actorId) &&
        Objects.equals(this.filmId, filmActor.filmId) &&
        Objects.equals(this.lastUpdate, filmActor.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(actorId, filmId, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FilmActor {\n");
    
    sb.append("    actorId: ").append(toIndentedString(actorId)).append("\n");
    sb.append("    filmId: ").append(toIndentedString(filmId)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

