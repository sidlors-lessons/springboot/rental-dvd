package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Inventory
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class Inventory   {
  @JsonProperty("inventory_id")
  private Integer inventoryId = null;

  @JsonProperty("film_id")
  private Integer filmId = null;

  @JsonProperty("store_id")
  private Integer storeId = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public Inventory inventoryId(Integer inventoryId) {
    this.inventoryId = inventoryId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/>
   * @return inventoryId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/>")
  @NotNull


  public Integer getInventoryId() {
    return inventoryId;
  }

  public void setInventoryId(Integer inventoryId) {
    this.inventoryId = inventoryId;
  }

  public Inventory filmId(Integer filmId) {
    this.filmId = filmId;
    return this;
  }

  /**
   * Note: This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>
   * @return filmId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>")
  @NotNull


  public Integer getFilmId() {
    return filmId;
  }

  public void setFilmId(Integer filmId) {
    this.filmId = filmId;
  }

  public Inventory storeId(Integer storeId) {
    this.storeId = storeId;
    return this;
  }

  /**
   * Get storeId
   * @return storeId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getStoreId() {
    return storeId;
  }

  public void setStoreId(Integer storeId) {
    this.storeId = storeId;
  }

  public Inventory lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Inventory inventory = (Inventory) o;
    return Objects.equals(this.inventoryId, inventory.inventoryId) &&
        Objects.equals(this.filmId, inventory.filmId) &&
        Objects.equals(this.storeId, inventory.storeId) &&
        Objects.equals(this.lastUpdate, inventory.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(inventoryId, filmId, storeId, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Inventory {\n");
    
    sb.append("    inventoryId: ").append(toIndentedString(inventoryId)).append("\n");
    sb.append("    filmId: ").append(toIndentedString(filmId)).append("\n");
    sb.append("    storeId: ").append(toIndentedString(storeId)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

