package mx.sidlors.dvd.rental.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * FilmCategory
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")


public class FilmCategory   {
  @JsonProperty("film_id")
  private Integer filmId = null;

  @JsonProperty("category_id")
  private Integer categoryId = null;

  @JsonProperty("last_update")
  private String lastUpdate = null;

  public FilmCategory filmId(Integer filmId) {
    this.filmId = filmId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/> This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>
   * @return filmId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/> This is a Foreign Key to `film.film_id`.<fk table='film' column='film_id'/>")
  @NotNull


  public Integer getFilmId() {
    return filmId;
  }

  public void setFilmId(Integer filmId) {
    this.filmId = filmId;
  }

  public FilmCategory categoryId(Integer categoryId) {
    this.categoryId = categoryId;
    return this;
  }

  /**
   * Note: This is a Primary Key.<pk/> This is a Foreign Key to `category.category_id`.<fk table='category' column='category_id'/>
   * @return categoryId
  **/
  @ApiModelProperty(required = true, value = "Note: This is a Primary Key.<pk/> This is a Foreign Key to `category.category_id`.<fk table='category' column='category_id'/>")
  @NotNull


  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public FilmCategory lastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
    return this;
  }

  /**
   * Get lastUpdate
   * @return lastUpdate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FilmCategory filmCategory = (FilmCategory) o;
    return Objects.equals(this.filmId, filmCategory.filmId) &&
        Objects.equals(this.categoryId, filmCategory.categoryId) &&
        Objects.equals(this.lastUpdate, filmCategory.lastUpdate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(filmId, categoryId, lastUpdate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FilmCategory {\n");
    
    sb.append("    filmId: ").append(toIndentedString(filmId)).append("\n");
    sb.append("    categoryId: ").append(toIndentedString(categoryId)).append("\n");
    sb.append("    lastUpdate: ").append(toIndentedString(lastUpdate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

