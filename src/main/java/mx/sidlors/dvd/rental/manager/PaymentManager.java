package mx.sidlors.dvd.rental.manager;

import mx.sidlors.dvd.rental.entity.Payment;
import mx.sidlors.dvd.rental.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
* Generated by Spring Data Generator on 20/07/2021
*/
@Component
public class PaymentManager {

	private PaymentRepository paymentRepository;

	@Autowired
	public PaymentManager(PaymentRepository paymentRepository) {
		this.paymentRepository = paymentRepository;
	}

}
