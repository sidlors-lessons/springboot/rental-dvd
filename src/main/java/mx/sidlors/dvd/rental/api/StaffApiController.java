package mx.sidlors.dvd.rental.api;

import  mx.sidlors.dvd.rental.model.Staff;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")

@Controller
public class StaffApiController implements StaffApi {

    private static final Logger log = LoggerFactory.getLogger(StaffApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public StaffApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> staffDelete(@ApiParam(value = "") @Valid @RequestParam(value = "staff_id", required = false) String staffId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "") @Valid @RequestParam(value = "username", required = false) String username,@ApiParam(value = "") @Valid @RequestParam(value = "password", required = false) String password,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "picture", required = false) String picture,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Staff>> staffGet(@ApiParam(value = "") @Valid @RequestParam(value = "staff_id", required = false) String staffId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "") @Valid @RequestParam(value = "username", required = false) String username,@ApiParam(value = "") @Valid @RequestParam(value = "password", required = false) String password,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "picture", required = false) String picture,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Ordering") @Valid @RequestParam(value = "order", required = false) String order,@ApiParam(value = "Limiting and Pagination" ) @RequestHeader(value="Range", required=false) String range,@ApiParam(value = "Limiting and Pagination" , defaultValue="items") @RequestHeader(value="Range-Unit", required=false) String rangeUnit,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "offset", required = false) String offset,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "limit", required = false) String limit,@ApiParam(value = "Preference" , allowableValues="count=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Staff>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Staff>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Staff>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> staffPatch(@ApiParam(value = "") @Valid @RequestParam(value = "staff_id", required = false) String staffId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "") @Valid @RequestParam(value = "username", required = false) String username,@ApiParam(value = "") @Valid @RequestParam(value = "password", required = false) String password,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "picture", required = false) String picture,@ApiParam(value = "staff"  )  @Valid @RequestBody Staff staff,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> staffPost(@ApiParam(value = "staff"  )  @Valid @RequestBody Staff staff,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
