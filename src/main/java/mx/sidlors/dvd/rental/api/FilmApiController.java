package mx.sidlors.dvd.rental.api;

import  mx.sidlors.dvd.rental.model.Film;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")

@Controller
public class FilmApiController implements FilmApi {

    private static final Logger log = LoggerFactory.getLogger(FilmApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public FilmApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> filmDelete(@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "title", required = false) String title,@ApiParam(value = "") @Valid @RequestParam(value = "description", required = false) String description,@ApiParam(value = "") @Valid @RequestParam(value = "release_year", required = false) String releaseYear,@ApiParam(value = "") @Valid @RequestParam(value = "language_id", required = false) String languageId,@ApiParam(value = "") @Valid @RequestParam(value = "rental_duration", required = false) String rentalDuration,@ApiParam(value = "") @Valid @RequestParam(value = "rental_rate", required = false) String rentalRate,@ApiParam(value = "") @Valid @RequestParam(value = "length", required = false) String length,@ApiParam(value = "") @Valid @RequestParam(value = "replacement_cost", required = false) String replacementCost,@ApiParam(value = "") @Valid @RequestParam(value = "rating", required = false) String rating,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "special_features", required = false) String specialFeatures,@ApiParam(value = "") @Valid @RequestParam(value = "fulltext", required = false) String fulltext,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Film>> filmGet(@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "title", required = false) String title,@ApiParam(value = "") @Valid @RequestParam(value = "description", required = false) String description,@ApiParam(value = "") @Valid @RequestParam(value = "release_year", required = false) String releaseYear,@ApiParam(value = "") @Valid @RequestParam(value = "language_id", required = false) String languageId,@ApiParam(value = "") @Valid @RequestParam(value = "rental_duration", required = false) String rentalDuration,@ApiParam(value = "") @Valid @RequestParam(value = "rental_rate", required = false) String rentalRate,@ApiParam(value = "") @Valid @RequestParam(value = "length", required = false) String length,@ApiParam(value = "") @Valid @RequestParam(value = "replacement_cost", required = false) String replacementCost,@ApiParam(value = "") @Valid @RequestParam(value = "rating", required = false) String rating,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "special_features", required = false) String specialFeatures,@ApiParam(value = "") @Valid @RequestParam(value = "fulltext", required = false) String fulltext,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Ordering") @Valid @RequestParam(value = "order", required = false) String order,@ApiParam(value = "Limiting and Pagination" ) @RequestHeader(value="Range", required=false) String range,@ApiParam(value = "Limiting and Pagination" , defaultValue="items") @RequestHeader(value="Range-Unit", required=false) String rangeUnit,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "offset", required = false) String offset,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "limit", required = false) String limit,@ApiParam(value = "Preference" , allowableValues="count=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Film>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Film>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Film>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> filmPatch(@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "title", required = false) String title,@ApiParam(value = "") @Valid @RequestParam(value = "description", required = false) String description,@ApiParam(value = "") @Valid @RequestParam(value = "release_year", required = false) String releaseYear,@ApiParam(value = "") @Valid @RequestParam(value = "language_id", required = false) String languageId,@ApiParam(value = "") @Valid @RequestParam(value = "rental_duration", required = false) String rentalDuration,@ApiParam(value = "") @Valid @RequestParam(value = "rental_rate", required = false) String rentalRate,@ApiParam(value = "") @Valid @RequestParam(value = "length", required = false) String length,@ApiParam(value = "") @Valid @RequestParam(value = "replacement_cost", required = false) String replacementCost,@ApiParam(value = "") @Valid @RequestParam(value = "rating", required = false) String rating,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "special_features", required = false) String specialFeatures,@ApiParam(value = "") @Valid @RequestParam(value = "fulltext", required = false) String fulltext,@ApiParam(value = "film"  )  @Valid @RequestBody Film film,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> filmPost(@ApiParam(value = "film"  )  @Valid @RequestBody Film film,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
