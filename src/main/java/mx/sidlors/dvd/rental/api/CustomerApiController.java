package mx.sidlors.dvd.rental.api;

import  mx.sidlors.dvd.rental.model.Customer;
import org.threeten.bp.LocalDate;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")

@Controller
public class CustomerApiController implements CustomerApi {

    private static final Logger log = LoggerFactory.getLogger(CustomerApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public CustomerApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> customerDelete(@ApiParam(value = "") @Valid @RequestParam(value = "customer_id", required = false) String customerId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "activebool", required = false) String activebool,@ApiParam(value = "") @Valid @RequestParam(value = "create_date", required = false) LocalDate createDate,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Customer>> customerGet(@ApiParam(value = "") @Valid @RequestParam(value = "customer_id", required = false) String customerId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "activebool", required = false) String activebool,@ApiParam(value = "") @Valid @RequestParam(value = "create_date", required = false) LocalDate createDate,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Ordering") @Valid @RequestParam(value = "order", required = false) String order,@ApiParam(value = "Limiting and Pagination" ) @RequestHeader(value="Range", required=false) String range,@ApiParam(value = "Limiting and Pagination" , defaultValue="items") @RequestHeader(value="Range-Unit", required=false) String rangeUnit,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "offset", required = false) String offset,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "limit", required = false) String limit,@ApiParam(value = "Preference" , allowableValues="count=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Customer>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Customer>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Customer>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> customerPatch(@ApiParam(value = "") @Valid @RequestParam(value = "customer_id", required = false) String customerId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "first_name", required = false) String firstName,@ApiParam(value = "") @Valid @RequestParam(value = "last_name", required = false) String lastName,@ApiParam(value = "") @Valid @RequestParam(value = "email", required = false) String email,@ApiParam(value = "") @Valid @RequestParam(value = "address_id", required = false) String addressId,@ApiParam(value = "") @Valid @RequestParam(value = "activebool", required = false) String activebool,@ApiParam(value = "") @Valid @RequestParam(value = "create_date", required = false) LocalDate createDate,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "") @Valid @RequestParam(value = "active", required = false) String active,@ApiParam(value = "customer"  )  @Valid @RequestBody Customer customer,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> customerPost(@ApiParam(value = "customer"  )  @Valid @RequestBody Customer customer,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
