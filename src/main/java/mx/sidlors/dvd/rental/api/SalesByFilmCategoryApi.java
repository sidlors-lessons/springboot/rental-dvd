/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.21).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package mx.sidlors.dvd.rental.api;

import  mx.sidlors.dvd.rental.model.SalesByFilmCategory;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")

@Validated
@Api(value = "sales_by_film_category", description = "the sales_by_film_category API")
@RequestMapping(value = "")
public interface SalesByFilmCategoryApi {

    @ApiOperation(value = "", nickname = "salesByFilmCategoryGet", notes = "", response = SalesByFilmCategory.class, responseContainer = "List", tags={ "sales_by_film_category", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = SalesByFilmCategory.class, responseContainer = "List"),
        @ApiResponse(code = 206, message = "Partial Content") })
    @RequestMapping(value = "/sales_by_film_category",
        produces = { "application/json", "application/vnd.pgrst.object+json", "text/csv" }, 
        consumes = { "application/json", "application/vnd.pgrst.object+json", "text/csv" },
        method = RequestMethod.GET)
    ResponseEntity<List<SalesByFilmCategory>> salesByFilmCategoryGet(@ApiParam(value = "") @Valid @RequestParam(value = "category", required = false) String category,@ApiParam(value = "") @Valid @RequestParam(value = "total_sales", required = false) String totalSales,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Ordering") @Valid @RequestParam(value = "order", required = false) String order,@ApiParam(value = "Limiting and Pagination" ) @RequestHeader(value="Range", required=false) String range,@ApiParam(value = "Limiting and Pagination" , defaultValue="items") @RequestHeader(value="Range-Unit", required=false) String rangeUnit,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "offset", required = false) String offset,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "limit", required = false) String limit,@ApiParam(value = "Preference" , allowableValues="count=none") @RequestHeader(value="Prefer", required=false) String prefer);

}
