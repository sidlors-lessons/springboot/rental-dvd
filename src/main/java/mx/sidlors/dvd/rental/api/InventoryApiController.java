package mx.sidlors.dvd.rental.api;

import  mx.sidlors.dvd.rental.model.Inventory;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-07-21T02:33:33.560Z")

@Controller
public class InventoryApiController implements InventoryApi {

    private static final Logger log = LoggerFactory.getLogger(InventoryApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public InventoryApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> inventoryDelete(@ApiParam(value = "") @Valid @RequestParam(value = "inventory_id", required = false) String inventoryId,@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Inventory>> inventoryGet(@ApiParam(value = "") @Valid @RequestParam(value = "inventory_id", required = false) String inventoryId,@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Ordering") @Valid @RequestParam(value = "order", required = false) String order,@ApiParam(value = "Limiting and Pagination" ) @RequestHeader(value="Range", required=false) String range,@ApiParam(value = "Limiting and Pagination" , defaultValue="items") @RequestHeader(value="Range-Unit", required=false) String rangeUnit,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "offset", required = false) String offset,@ApiParam(value = "Limiting and Pagination") @Valid @RequestParam(value = "limit", required = false) String limit,@ApiParam(value = "Preference" , allowableValues="count=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Inventory>>(objectMapper.readValue("{}", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Inventory>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Inventory>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> inventoryPatch(@ApiParam(value = "") @Valid @RequestParam(value = "inventory_id", required = false) String inventoryId,@ApiParam(value = "") @Valid @RequestParam(value = "film_id", required = false) String filmId,@ApiParam(value = "") @Valid @RequestParam(value = "store_id", required = false) String storeId,@ApiParam(value = "") @Valid @RequestParam(value = "last_update", required = false) String lastUpdate,@ApiParam(value = "inventory"  )  @Valid @RequestBody Inventory inventory,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> inventoryPost(@ApiParam(value = "inventory"  )  @Valid @RequestBody Inventory inventory,@ApiParam(value = "Filtering Columns") @Valid @RequestParam(value = "select", required = false) String select,@ApiParam(value = "Preference" , allowableValues="return=representation, return=minimal, return=none") @RequestHeader(value="Prefer", required=false) String prefer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
